package delfia.puja.uts_123_101

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class login2 : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        login.setOnClickListener(this)
        regis.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.login->{
                var email = email.text.toString()
                var password = password.text.toString()
                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this,"Username / Password can't be empty ",
                        Toast.LENGTH_SHORT).show()
                }else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener {
                            if (!it.isSuccessful) return@addOnCompleteListener
                            progressDialog.hide()
                            Toast.makeText(this, "Succesfully Login",Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, MainActivity ::class.java)
                            startActivity(intent)

                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this,"Username /password incorrect",
                                Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.regis->{
                val intent = Intent(this,register2::class.java)
                startActivity(intent)
            }
        }
    }
}