package delfia.puja.uts_123_101

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    var fbaut= FirebaseAuth.getInstance()
    lateinit var viewDial : View
    val Tag = "openfcm"
    var id =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loc.setOnClickListener(this)
        file.setOnClickListener(this)
        ins.setOnClickListener(this)
        logout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.file->{
                val intent = Intent(this, file2::class.java)
                startActivity(intent)
            }
            R.id.ins->{
                val intent = Intent(this, pengajar2::class.java)
                startActivity(intent)
            }
            R.id.loc->{
                val intent = Intent(this, maps::class.java)
                startActivity(intent)
            }
            R.id.logout->{
                out("Ingin Keluar Dari Aplikasi !!!")
            }
        }
    }

    fun out(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Warning")
            .setContentText(string)
            .setConfirmText("Iya")
            .setCancelText("Tidak")
            .setConfirmClickListener { sDialog -> sDialog.dismissWithAnimation()
                fbaut.signOut()
                finish()
            }
            .setCancelClickListener{ sDialog -> sDialog.dismissWithAnimation()
            }
            .show()
    }

}
